import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('TrackPlayerTest', () => App);
AppRegistry.registerHeadlessTask('TrackPlayer', () => require('./player-handler.js'));
