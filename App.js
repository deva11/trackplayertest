/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
    TouchableOpacity
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
import TrackPlayer from 'react-native-track-player';
import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource';

type Props = {};
export default class App extends Component<Props> {



  componentWillMount()
  {
      TrackPlayer.setupPlayer().then(async () => {

          // Adds a track to the queue
          await TrackPlayer.add({
              id: '11',


              url: resolveAssetSource(require('./Assets/veena.mp3')), // Load media from the app bundle

              title: 'Humpi',
              artist: 'AR',
              album: 'Carnival of Carvings',
              genre: 'Melody',
              date: '2018-01-20T07:00:00+00:00', // RFC 3339


              artwork: require('./Assets/sproutingseed.jpg'), // Load artwork from the app bundle
          });

          // Starts playing it
          // TrackPlayer.play();

      });
  }
    onPressPlay = () => {
        TrackPlayer.play();
    };

  onPressPause = () => {
        TrackPlayer.pause();
    };

  render() {
      let pick = resolveAssetSource(require('./Assets/veena.mp3'));
      var x = JSON.stringify(pick);
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          TrackPlayer Demo
        </Text>
        <Text style={styles.instructions}>
          Play and Pause TrackPlayer
        </Text>
          <View style= {{flexDirection :'column'}}>


        <TouchableOpacity style= {{borderRadius:20,width:100,height:50,backgroundColor:'red',justifyContent:'center',alignItems:'center'}} onPress = {this.onPressPlay}>
            <Text style = {{fontWeight:'bold',textAlign:'center',color:'white'}}>Play Me</Text>
        </TouchableOpacity>
        <TouchableOpacity style= {{borderRadius:20,marginTop:20,width:100,height:50,backgroundColor:'blue',justifyContent:'center',alignItems:'center'}} onPress = {this.onPressPause}>
            <Text style = {{fontWeight:'bold',textAlign:'center',color:'white'}}>Pause Me</Text>
        </TouchableOpacity>
          </View>

          <Text style= {{marginTop:20,textAlign:'center'}}>

                  {`Value = ${x}`}

          </Text>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
